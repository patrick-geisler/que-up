import { addSongToQue,
   skipSong as skipSongQue,
    getHost as getHostQue,
    getTrackList as getTrackListQue,
    removeFromPlaylist as removeFromPlaylistQue
   } from '../utils/que'
// add search artist

// add song

// next song

export const searchSong = song => (dispatch, getState, spotifyApi) => {
  dispatch({ type: 'SEARCH_SONG', payload: song })

  if (song.length < 3) {
    return Promise.resolve()  // todo: dispach an action to clean out the search list
  }
  return spotifyApi.searchTracks(song)
    .then((tracks) => {
      return dispatch({ type: 'SEARCH_SONG_SUCCESS', payload: {tracks} })
    })
    .catch(err => {
      return dispatch({ type: 'SEARCH_SONG_ERROR', payload: err })
    })
}

const fetchCreator = (actionType, action, actionArgs) => (dispatch, getState, spotifyApi) => {
  dispatch({ type: actionType, payload: actionArgs })
  return action(actionArgs)
    .then(response => {
      dispatch({ type: `${actionType}_SUCCESS`, payload: response})
    }) // if you wanna do any formatting do in the util
    .catch(err => dispatch({ type: `${actionType}_ERROR`, payload: err }))
}

export const addSong = song => (dispatch, getState, spotifyApi) => {
  dispatch({type: 'ADD_SONG'})
  addSongToQue(song)
    .then(res => dispatch({ type: 'ADD_SONG_SUCCESS' }))
    .catch(err => dispatch({ type: 'ADD_SONG_ERROR', payload: err}))
}

export const removeFromPlaylist = song => (dispatch, getState, spotifyApi) => {
  dispatch({type: 'REMOVE_SONG_FROM_PLAYLIST'})
  removeFromPlaylistQue(song)
    .then(res => dispatch({ type: 'REMOVE_SONG_FROM_PLAYLIST_SUCCESS' }))
    .catch(err => dispatch({ type: 'REMOVE_SONG_FROM_PLAYLIST_ERROR', payload: err}))
}

// todo: add a creator for api actions that just adds _SUCCESS & _ERROR to the action type just takes in
export const skipSong = () => (dispatch, getState) => {
  dispatch({type: 'SKIP_SONG'})
  return skipSongQue()
    .then(res => dispatch({type: 'SKIP_SONG_SUCESS'}))
    .catch(err => {
      console.log(err)
      dispatch({type: 'SKIP_SONG_ERROR', payload: err})
    })
}
// todo: move que singleton into the store actions
export const getHost = () => fetchCreator('GET_HOST', getHostQue, null)

export const getGuest = () => (dispatch, getState, spotifyApi) => {
  dispatch({ type: 'GET_GUEST'})
  return spotifyApi.getMe()
    .then(res => dispatch({type: 'GET_GUEST_SUCCESS', payload: res}))
    .catch(err => {
      console.log(err)
      dispatch({ type: 'GET_GUEST_ERROR' })
    })
}

export const getTrackList = () => (dispatch, getState, spotifyApi) => {
  dispatch({ type: 'GET_TRACK_LIST' })
  return getTrackListQue()
    .then(res => res.json())
    .then(res => {    
      console.log(res)
      return res
    })
    .then(res => dispatch({type: 'GET_TRACK_LIST_SUCCESS', payload: res}))
    .catch(err => {
      console.log(err)
      dispatch({ type: 'GET_TRACK_LIST_ERROR' })
    })
}

// export const fetchInitalState = () =>
//   Promise.all([getTrackList(), getHost(), getGuest()])
