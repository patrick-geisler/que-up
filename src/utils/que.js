/* global fetch */
const queURL = process.env.NODE_ENV === 'development' ? 'http://localhost:8080' : 'https://damp-hollows-67395.herokuapp.com' // todo: replace with public url

export const addSongToQue = (songURI) =>
    fetch(`${queURL}/playlist/addToPlaylist/${songURI}`, {method: 'POST'})

export const skipSong = () =>
    fetch(`${queURL}/playback/skip`, {method: 'GET'})

export const getHost = () =>
    fetch(`${queURL}/me`)
        .then(response => response.json())

export const getTrackList = () =>
    fetch(`${queURL}/playlist/thePlaylistTrackList`, {method: 'GET'})

export const removeFromPlaylist = (songURI) => 
    fetch(`${queURL}/playlist/removeFromPlaylist/${songURI}`, {method: 'POST'})
