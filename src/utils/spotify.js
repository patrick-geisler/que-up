import SpotifyApi from 'spotify-web-api-js'

export const getHashParams = () => {
  const hashParams = {}
  let e
  let r = /([^&;=]+)=?([^&;]*)/g
  let q = window.location.hash.substring(1)
  while (e = r.exec(q)) {  // eslint-disable-line
    hashParams[e[1]] = decodeURIComponent(e[2])
  }
  return hashParams
}

class Spotify {
  constructor () {
    const hashedParams = getHashParams()
    if (hashedParams.access_token) {
      this.api = new SpotifyApi()
      this.api.setAccessToken(hashedParams.access_token)
      window.localStorage.setItem('access_token', hashedParams.access_token)
    } else if (window.localStorage.getItem('access_token')) {
      this.api = new SpotifyApi()
      this.api.setAccessToken(window.localStorage.getItem('access_token'))
    } else {
      if (window.location.pathname !== '#/login') {
        window.location.hash = '/login'
      }
    }
  }

  searchTracks (searchString) {
    return this.api.searchTracks(searchString)
      .then(response => response.tracks)
  }

  getMe () {
    return this.api.getMe()
  }

  reauth () {
    console.log('reauth')
  }
}

export default new Spotify()
