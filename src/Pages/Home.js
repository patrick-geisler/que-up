import React, {Component} from 'react'
import { connect } from 'react-redux'
import { getGuest as getGuestAction, getHost as getHostAction } from '../store/actions'
import User from '../Components/User'
import Subheader from 'material-ui/Subheader'

class Home extends Component {
  componentDidMount () {
    if (!this.props.guest.display_name) {
      this.props.getGuest()
    }
    if (!this.props.host.display_name) {
      this.props.getHost()
    }
  }
  render () {
    if (!this.props.guest.display_name) return null
    return (
      <div>
        <Subheader>You</Subheader>
        <User host={this.props.guest} />
        <Subheader>Host</Subheader>
        <User host={this.props.host} />
      </div>
    )
  }
}

export default connect(
  (state) => ({
    guest: state.spotify.guest,
    host: state.que.host
  }),
  (dispatch) => ({
    getGuest: () => dispatch(getGuestAction()),
    getHost: () => dispatch(getHostAction())
  })
)(Home)
