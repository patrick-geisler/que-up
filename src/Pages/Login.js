import React from 'react'
import RaisedButton from 'material-ui/RaisedButton'

const redirectUrl = process.env.NODE_ENV === 'development' ? 'http://localhost:3000' : 'https://que-up.surge.sh'
const config = {
  redirectUrl,
  client_id: 'f374e19cfd4b41dcab8da902a4540dca',
  response_type: 'token'
}

const authUrl = `https://accounts.spotify.com/authorize?redirect_uri=${config.redirectUrl}&client_id=${config.client_id}&response_type=${config.response_type}`

export default () => {
  return (
    <div>
      <RaisedButton label='Login' fullWidth onClick={(e) => {
        console.log('login')
        window.location = authUrl
      }} />
    </div>
  )
}
