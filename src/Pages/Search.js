import React from 'react'
import Search from '../Components/Search'
import Songs from '../Components/Songs'
import {connect} from 'react-redux'

const SearchPage = ({songs}) =>
  <div>
    <Search />
    <Songs songs={songs} />
  </div>

export default connect((state) => ({
  songs: state.spotify.tracks.items
}))(SearchPage)
