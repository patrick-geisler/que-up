import React, { Component } from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import { connect } from 'react-redux'
import { getHost as getHostAction } from '../store/actions'
import User from '../Components/User'

const authUrl = process.env.NODE_ENV === 'development' ? 'http://localhost:8080/login' : 'https://damp-hollows-67395.herokuapp.com/login' // todo

class Host extends Component {
    // constructor (props) {
    //     super(props)
    //     // component did mount to getHost
    //     // todo: create que reducer and get the img of the Host and display it
    // }

  componentDidMount () {
    this.props.getHost()
  }

  render () {
    return (
      <div>
        <RaisedButton label='Be the host' fullWidth onClick={(e) => {
          window.location = authUrl
        }} />
        <User host={this.props.host} />
      </div>
    )
  }
}

export default connect(
    (state) => ({
      host: state.que.host,
      guest: state.spotify.guest
    }),
    (dispatch) => ({
      getHost: () => dispatch(getHostAction())
    })
  )(Host)
