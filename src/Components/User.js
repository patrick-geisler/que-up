import React from 'react'
import { Card, CardHeader } from 'material-ui/Card'

const User = ({host}) => {
  if (!host.display_name) return <div>No user sent be the host!</div>
  return (
    <Card onClick={() => window.location = host.external_urls.spotify}>
      <CardHeader
        title={host.display_name}
        subtitle={host.product + ' user'}
        avatar={host.images[0].url}
            />
    </Card>)
}

export default User
