import React from 'react'
import { List } from 'material-ui/List'
import Divider from 'material-ui/Divider'
import Subheader from 'material-ui/Subheader'

import Track from './Track'

const TrackList = ({songs = []}) => (
  <div>
    <List>
      <Subheader>Up Next</Subheader>
      {songs.map((song, key) => <Track key={key} song={song} />)}
      <Divider inset />
    </List>
  </div>
)

export default TrackList
