import React from 'react'
import { ListItem } from 'material-ui/List'
import { lightBlack } from 'material-ui/styles/colors'
import Avatar from 'material-ui/Avatar'
import { connect } from 'react-redux'

// todo: add voting system
const Track = ({ song, addSong }) => {
  return <ListItem
    primaryText={song.name}
    leftAvatar={<Avatar src={song.album.images[0].url} />}
    secondaryText={
      <p>
        <span style={{color: lightBlack}}>{song.artists.map(artist => `${artist.name} `)}</span>
      </p>
    }
    secondaryTextLines={1}
  />
}

export default connect(
  (state) => ({}),
  (dispatch) => ({
    // addSong: (song) => dispatch(addSong(song))
  }))(Track)
