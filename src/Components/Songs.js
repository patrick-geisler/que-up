import React from 'react'
import { List } from 'material-ui/List'
import Divider from 'material-ui/Divider'
import Subheader from 'material-ui/Subheader'

import Song from './Song'

const Songs = ({songs = []}) => (
  <div>
    <List>
      <Subheader>Songs</Subheader>
      {songs.map((song, key) => <Song key={key} song={song} />)}
      <Divider inset />
    </List>
  </div>
)

export default Songs
