import React, { Component } from 'react'
import { ListItem } from 'material-ui/List'
import { lightBlack } from 'material-ui/styles/colors'
import Avatar from 'material-ui/Avatar'
import { addSong, removeFromPlaylist as removeFromPlaylistAction } from '../store/actions'
import { connect } from 'react-redux'
import Snackbar from 'material-ui/Snackbar'

class Song extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      autoHideDuration: 4000,
      message: `Added!`,
      open: false,
    };
  }

  handleTouchTap = () => {
    this.setState({
      open: true,
    });
  };

  handleActionTouchTap = () => {
    this.setState({
      open: false,
    });
    this.props.removeFromPlaylist(this.props.song.uri)
  };

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  render() {
    return (
      <div>
        <ListItem
          onClick={() => {
              this.handleTouchTap()
              this.props.addSong(this.props.song.uri)}
            }
          primaryText={this.props.song.name}
          leftAvatar={<Avatar src={this.props.song.album.images[0].url} />}
          secondaryText={
            <p>
              <span style={{color: lightBlack}}>{this.props.song.artists.map(artist => `${artist.name} `)}</span>
            </p>
          }
          secondaryTextLines={1}
        />
        <Snackbar
            open={this.state.open}
            message={this.state.message}
            action="undo"
            autoHideDuration={this.state.autoHideDuration}
            onActionTouchTap={this.handleActionTouchTap}
            onRequestClose={this.handleRequestClose}
          />
    </div>)
  }
}

export default connect(
  (state) => ({}),
  (dispatch) => ({
    addSong: (song) => dispatch(addSong(song)),
    removeFromPlaylist: (song) => dispatch(removeFromPlaylistAction(song))
  }))(Song)
